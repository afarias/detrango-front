# DetranGo - Frontend

Este projeto é gerado no [Angular CLI](https://github.com/angular/angular-cli) versão 8.0.1

## Pré-Requisitos de Softwares
Minimo de 2 GB de espaço livre 
Node Js - Versão 12+
Git 2+
npm install -g typescript
npm install -g @angular/cli

## Servidor de Desenvolvimento

Rodar `npm install` para instalar as dependencias
Rodar `ng serve` rodar o servidor de desenvolvimento. Navegar até `http://localhost:4200/`. O Aplicativo atualiza automaticamente com qualquer modificação.

## Gerando Build

Rodar `ng build` para buildar o projeto. O artefato de build se encontrará no diretório `dist/`. Use `--prod` para buildar para produção.

