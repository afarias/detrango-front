const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const port = process.env.PORT || 7501;

app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression());
app.use(express.static('dist'));

app.get('*', function(req, res) {
  res.sendFile('./dist/index.html', {root : __dirname});
});

app.listen(port, () => {
  console.log(`Server started on port ${port}.`);
});
