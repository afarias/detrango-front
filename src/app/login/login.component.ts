﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.minLength(6))
  });

  public passwordFieldType: string = 'password';
  public iconChangePasswordField: string = 'la la-eye';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    public authService: AuthService,
    private renderer: Renderer2,
    private toastr: ToastrService,
  ) {
    this.route.queryParams.subscribe(params => {
      this.returnUrl = params['returnUrl'];
    });

  }

  ngOnInit() {

  }

  get f() {
    return this.loginForm.controls;
  }

  tryLogin() {
    this.submitted = true;
    const value = {
      email: this.f.username.value,
      password: this.f.password.value
    };

    this.authService.usernameAndPasswordLogin(value).subscribe((result) => {

      sessionStorage.setItem('auth', JSON.stringify(result));
      sessionStorage.setItem('userToken', result['token']);

      sessionStorage.setItem('permissions', result['roles']);
      sessionStorage.setItem('acesso', "0");

      sessionStorage.setItem('currentUser', JSON.stringify(null));

      sessionStorage.setItem('acesso', "0");
      this.router.navigate(['/detran-go/tipo-item']);



    }, (err) => {
      this.toastr.error(err.error.message, 'Erro', {
        timeOut: 5000,
      })
    });
  }


  setUserInStorage(res) {

    if (res.user) {
      sessionStorage.setItem('currentUser', JSON.stringify(res.user));
    } else {
      sessionStorage.setItem('currentUser', JSON.stringify(res));
    }
  }

  showOrHidePassword() {
    this.passwordFieldType = this.passwordFieldType == 'password' ? 'text' : 'password';
    this.iconChangePasswordField = this.iconChangePasswordField == 'la la-eye' ? 'la la-eye-slash' : 'la la-eye';
  }
}
