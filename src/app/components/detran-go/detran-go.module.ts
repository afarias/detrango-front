import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import {TableModule} from 'primeng/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMaskInputMode, NgxCurrencyModule } from "ngx-currency";
import { TipoItemComponent } from './tipo-item/tipo-item.component';
import { TipoItemCadastroComponent } from './tipo-item-cadastro/tipo-item-cadastro.component';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

export const customCurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: ".",
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.FINANCIAL
};



@NgModule({
  declarations: [
    TipoItemComponent, 
    TipoItemCadastroComponent
  ],
  imports: [ 
    CommonModule,
    RouterModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule, 
    NgxMaskModule.forRoot(maskConfig),
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    NgbModule,
    RouterModule.forChild([
      {
        path: 'tipo-item',
        component: TipoItemComponent
      },
      {
        path: 'incluir-tipo-item',
        component: TipoItemCadastroComponent
      }
    ])
  ]
})
export class DetranGoModule { }
