import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { TipoItemService } from 'src/app/_services/tipo-item-service/tipo-item.service';


@Component({
  selector: 'app-tipo-item',
  templateUrl: './tipo-item.component.html',
  styleUrls: ['./tipo-item.component.scss']
})
export class TipoItemComponent implements OnInit {

  tipoItemList = [];
  public idTipoItem: any;

  public tipoItemForm = new FormGroup({
    tipoItem: new FormControl('', [Validators.required]),
    descricao: new FormControl('', [Validators.required]),
  })

  first = 0;

  rows = 10;

  constructor(
    private toastr: ToastrService,
    private tipoitemService: TipoItemService,
    private modalService: NgbModal,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.getTiposDeItens();
  }

  getTiposDeItens(tipoItem?, descricao?) {
    this.tipoItemList = [];

    this.tipoitemService.listarTipoDeItens(tipoItem, descricao, null, null).subscribe((result) => {
      result['content'].forEach(element => {
          this.tipoItemList.push(element);             
      });
      
      if(this.tipoItemList.length == 0){
        this.toastr.error("Nenhum registro encontrado para esta pesquisa.", 'Erro', {
          timeOut: 5000,
        })  
      }
    }, (err) => {
      this.toastr.error(err.error.message, 'Erro', {
        timeOut: 5000,
      })
    });
  }

  pesquisar() {
  if (this.tipoItemForm.controls['tipoItem'].value == "" || this.tipoItemForm.controls['tipoItem'].value == undefined) {
      this.toastr.error("Campo(s) obrigatório(s) não preenchidos.", 'Erro', {
        timeOut: 5000,
      })  
    }else {
      this.getTiposDeItens(this.tipoItemForm.controls['tipoItem'].value, this.tipoItemForm.controls['descricao'].value);
    }
  }

  limpar(){
    this.tipoItemForm.reset();
  }

  openModal(ModalContent, tipoItemId) {
    this.modalService.open(ModalContent, { windowClass: 'animated fadeInDown' });
    this.idTipoItem = tipoItemId;
  }

  excluirTipoDeItem(){
    this.tipoitemService.excluirTipoItem(this.idTipoItem).subscribe((result) => {
      this.toastr.success("Dados excluído com sucesso!", 'Sucesso', {
        timeOut: 5000,
      });
      this.modalService.dismissAll();
      this.getTiposDeItens();
    });
  }

  novo(){
    this.router.navigate(['/detran-go/incluir-tipo-item']);
    let tipoCadastro = {
      'cadastro': 0,
      'tipoItem': null
    }
    sessionStorage.setItem('tipoCadastro', JSON.stringify(tipoCadastro));
  }

  editar(tipoItem){
    this.router.navigate(['/detran-go/incluir-tipo-item']);
    let tipoCadastro = {
      'cadastro': 1,
      'tipoItem': tipoItem
    }
    sessionStorage.setItem('tipoCadastro', JSON.stringify(tipoCadastro));
  }

}
