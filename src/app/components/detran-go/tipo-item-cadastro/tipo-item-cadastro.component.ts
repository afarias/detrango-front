import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TipoItemService } from 'src/app/_services/tipo-item-service/tipo-item.service';


@Component({
  selector: 'app-tipo-item-cadastro',
  templateUrl: './tipo-item-cadastro.component.html',
  styleUrls: ['./tipo-item-cadastro.component.scss']
})
export class TipoItemCadastroComponent implements OnInit {

  tipoItemList = [];
  public idTipoItem: any;
  public tipoCadastro: any;

  public tipoItemForm = new FormGroup({
    tipoItem: new FormControl('', [Validators.required]),
    descricao: new FormControl('', [Validators.required]),
  })

  first = 0;

  rows = 10;

  constructor(
    private toastr: ToastrService,
    private tipoitemService: TipoItemService,
    private router: Router
  ) { }


  ngOnInit(): void {
    const tipoCadastro = JSON.parse(sessionStorage.getItem('tipoCadastro'));
    this.tipoCadastro = tipoCadastro;

    if(this.tipoCadastro.cadastro == 1){
      this.tipoItemForm.controls['tipoItem'].setValue(this.tipoCadastro.tipoItem.tipoItem);
      this.tipoItemForm.controls['descricao'].setValue(this.tipoCadastro.tipoItem.descricao);
    }
  }

  salvar() {
    if (this.tipoItemForm.controls['tipoItem'].value == "" || this.tipoItemForm.controls['descricao'].value == "" || this.tipoItemForm.controls['tipoItem'].value == undefined || this.tipoItemForm.controls['descricao'].value == undefined) {
      this.toastr.error("Campo(s) obrigatório(s) não preenchidos.", 'Erro', {
        timeOut: 5000,
      })
    } else {
      let tipoItem = {
        tipoItem: this.tipoItemForm.controls['tipoItem'].value,
        descricao: this.tipoItemForm.controls['descricao'].value
      }

      if (this.tipoCadastro.cadastro == 0) {
        this.tipoitemService.salvarTipoItem(tipoItem).subscribe((result) => {
          this.toastr.success("Dados salvos com sucesso.!", 'Sucesso', {
            timeOut: 5000,
          });
          this.router.navigate(['/detran-go/tipo-item']);
        }, (err) => {
          this.toastr.error(err.error.message, 'Erro', {
            timeOut: 5000,
          })
        });
      } else if (this.tipoCadastro.cadastro == 1) {
        this.tipoitemService.editarTipoItem(this.tipoCadastro.tipoItem.id, tipoItem).subscribe((result) => {
          this.toastr.success("Dados salvos com sucesso.!", 'Sucesso', {
            timeOut: 5000,
          });
          this.router.navigate(['/detran-go/tipo-item']);
        }, (err) => {
          this.toastr.error(err.error.message, 'Erro', {
            timeOut: 5000,
          })
        });
      }
    }
  }

  limpar() {
    this.tipoItemForm.reset();
  }

  cancelar() {
    this.router.navigate(['/detran-go/tipo-item']);
  }


}
