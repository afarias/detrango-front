import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { base_url } from '../../_configs/configurations';

@Injectable({
  providedIn: 'root'
})
export class TipoItemService {

  constructor(private httpClient: HttpClient, ) { }

  getUserToken(){
    return sessionStorage.getItem("userToken");
  }

  listarTipoDeItens(tipoItem, descricao, paginationLimit?, paginationCurrentPage?){
  
    let pesquisarTipoItem = "";
    let pesquisarDescricao = "";

    if(tipoItem != null){
      pesquisarTipoItem = `tipoItem=${tipoItem}&`;
    }

    if(descricao != null){
      pesquisarDescricao = `descricao=${descricao}&`;
    }

    return this.httpClient.get(`${base_url}tipoItem/listar?${pesquisarTipoItem}${pesquisarDescricao}page=${paginationCurrentPage}&size=${paginationLimit}&sort=tipoItem,asc`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.getUserToken()}`
      })
    });
  }

  excluirTipoItem(id){
    return this.httpClient.delete(`${base_url}tipoItem/excluir/${id}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.getUserToken()}`
      })
    })
  }

  salvarTipoItem(data){
    return this.httpClient.post(`${base_url}tipoItem/salvar`, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.getUserToken()}`
      })
    });
  }

  editarTipoItem(id, data){
    return this.httpClient.put(`${base_url}tipoItem/editar/${id}`, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.getUserToken()}`
      })
    })
  }

}
